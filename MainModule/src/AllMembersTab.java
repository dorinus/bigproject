import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;

public class AllMembersTab  extends Tab
{
  private VBox allMembersPane;
  private TextArea allMembersArea;
  private ScrollPane allMembersScrollPane;

  private Button getButton;

  private MyActionListener listener;

  private MemberFileAdapter adapter;

  /**
   * Constructor initializing the GUI components
   * @param title The title of the tab
   * @param adapter StudentFileAdapter object used for retrieving and storing student information
   */
  public AllMembersTab(String title, MemberFileAdapter adapter)
  {
    super(title);

    this.adapter = adapter;

    listener = new MyActionListener();

    allMembersArea = new TextArea();
    allMembersArea.setPrefRowCount(16);
    allMembersArea.setPrefColumnCount(50);
    allMembersArea.setEditable(false);

    allMembersScrollPane = new ScrollPane(allMembersArea);
    allMembersScrollPane.setFitToWidth(true);

    getButton = new Button("Get Members");
    getButton.setOnAction(listener);

    allMembersPane = new VBox(10);
    allMembersPane.setAlignment(Pos.CENTER);
    allMembersPane.getChildren().add(allMembersScrollPane);
    allMembersPane.getChildren().add(getButton);

    super.setContent( allMembersPane);
  }

  /**
   * Enables or disables editing of the allStudentsArea.
   * @param bool if true then the area will be editable, if false then it will not
   */
  public void changeEditableState(boolean bool)
  {
    allMembersArea.setEditable(bool);
  }

  /**
   * Updates the allStudentsArea TextArea with information from the students file
   */
  public void updateMemberArea()
  {
   MemberList members = adapter.getAllMembers();
    allMembersArea.setText(members.toString());
  }

  /*
   * Inner action listener class
   * @author Allan Henriksen
   * @version 3.0
   */
  private class MyActionListener implements EventHandler<ActionEvent>
  {
    public void handle(ActionEvent e)
    {
      if (e.getSource() == getButton)
      {
        updateMemberArea();
      }
    }
  }

}
