import java.io.Serializable;
import java.util.ArrayList;


public class MemberList implements Serializable
{
  private ArrayList<Member> members;

  /**
   * No-argument constructor initializing the MemberList.
   */
  public MemberList()
  {
    members = new ArrayList<Member>();
  }

  /**
   * Adds a member to the list.
   * @param member the member to add to the list
   */
  public void addMember(Member member)
  {
    members.add(member);
  }

  public void removeMember(Member member)
  {
    members.remove(member);
  }
  /**
   * Replaces the Member object at index with member.
   * @param member the member to replace with
   * @param index the position in the list that will be replaced
   */
  public void set(Member member, int index)
  {
    members.set(index, member);
  }

  /**
   * Gets a Member object from position index from the list.
   * @param index the position in the list of the Student object
   * @return the Member object at position index if one exists, else null
   */
  public Member get(int index)
  {
    if(index<members.size())
    {
      return members.get(index);
    }
    else
    {
      return null;
    }
  }

  /**
   * Gets a Member object with the given first name and last name from the list.
   * @param firstName the first name of the Student object
   * @param lastName the last name of the Student object
   * @return the Member object with the given first name and last name if one exists, else null
   */
  public Member get(String firstName, String lastName)
  {
    for(int i = 0; i<members.size(); i++)
    {
      Member temp = members.get(i);

      if(temp.getFirstName().equals(firstName) && temp.getLastName().equals(lastName))
      {
        return temp;
      }
    }

    return null;
  }

  /**
   * Gets the index of a Member object with the given first name and last name.
   * @param firstName the first name of the Member object
   * @param lastName the last name of the Member object
   * @return the index of the Student object with the given first name and last name if one exists, else -1
   */
  public int getIndex(String firstName, String lastName)
  {
    for(int i = 0; i<members.size(); i++)
    {
      Member temp = members.get(i);

      if(temp.getFirstName().equals(firstName) && temp.getLastName().equals(lastName))
      {
        return i;
      }
    }
    return -1;
  }

  /**
   * Gets how many Member objects are in the list.
   * @return the number of Member objects in the list
   */
  public int size()
  {
    return members.size();
  }

  /**
   * Gets a String representation of the MemberList.
   * @return a String containing information about all Member objects in the list - each Student object followed by a new line character
   */
  public String toString()
  {
    String returnStr = "";

    for(int i = 0; i<members.size(); i++)
    {
      Member temp = members.get(i);

      returnStr += temp +"\n";
    }
    return returnStr;
  }
}
