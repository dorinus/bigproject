import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;

//TODO make button that exports in xmls

public class AllScheduleTab extends Tab
{

  private VBox allSchedulePane;
  private TextArea allScheduleArea;
  private ScrollPane allScheduleScrollPane;


  private Button getButton;


  private MyActionListener listener;

  private ScheduleFileAdapter adapter;


  public AllScheduleTab(String title)
  {


    super(title);

    this.adapter = new ScheduleFileAdapter("MainModule/schedule.bin");

    listener = new MyActionListener();

    allScheduleArea = new TextArea();
    allScheduleArea.setPrefRowCount(16);
    allScheduleArea.setPrefColumnCount(50);
    allScheduleArea.setEditable(false);

    allScheduleScrollPane = new ScrollPane(allScheduleArea);
    allScheduleScrollPane.setFitToWidth(true);

    getButton = new Button("Update Schedule");
    getButton.setOnAction(listener);

    allSchedulePane = new VBox(10);
    allSchedulePane.setAlignment(Pos.CENTER);
    allSchedulePane.getChildren().add(allScheduleScrollPane);
    allSchedulePane.getChildren().add(getButton);

    super.setContent(allSchedulePane);

    updateScheduledArea(); // shows the schedules from the beg

  }

  /**
   * Enables or disables editing of the allStudentsArea.
   *
   * @param bool if true then the area will be editable, if false then it will not
   */
  public void changeEditableState(boolean bool)
  {
    allScheduleArea.setEditable(bool);
  }

  /**
   * Updates the allStudentsArea TextArea with information from the students file
   */
  public void updateScheduledArea()
  {

    Schedule schedule = adapter.getSchedule();
    allScheduleArea.setText(schedule.toString());
  }

  /*
   * Inner action listener class
   * @author Allan Henriksen
   * @version 3.0
   */
  private class MyActionListener implements EventHandler<ActionEvent>
  {
    public void handle(ActionEvent e)
    {
      if (e.getSource() == getButton)
      {
        updateScheduledArea();
      }
    }
  }

}



