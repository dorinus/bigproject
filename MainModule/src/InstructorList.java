import java.util.ArrayList;

public class InstructorList
{
  private ArrayList<Instructor> instructors;

  public InstructorList()
    {
      instructors = new ArrayList<>();
    }

    public void addInstructor(Instructor instructor)
    {
      instructors.add(instructor);
    }

    public void removeCInstructor(Instructor instructor)
    {
      instructors.remove(instructor);
    }

  public Instructor show(int phoneNumber)
  {
    Instructor temp = null;
    for (int i = 0; i < instructors.size(); i++)
    {
      if(instructors.get(i).getPhoneNumber() == phoneNumber)
      {
        temp = instructors.get(i);
        break;
      }
    }
    return temp;
  }

  public String toString()
  {
    String str = "";
    for (int i = 0; i < instructors.size(); i++)
    {
      str += instructors.get(i) + "\n";
    }
    return str;
  }
}
