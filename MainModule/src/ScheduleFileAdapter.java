//import java.io.Serializable;

public class ScheduleFileAdapter //implements Serializable
{
  private FileIO fileIO;
  private String fileName;

  public ScheduleFileAdapter(String fileName)
  {
    this.fileIO = new FileIO();
    this.fileName = fileName;
  }

public Schedule getSchedule(){
    return (Schedule) fileIO.readObjectFromFile(fileName);
}

public void writeSchedule(Schedule schedule){
    fileIO.write(fileName, schedule);
}


}
