import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class EditMemberTab extends Tab
{
  private VBox MemberPane;
  private HBox MemberTopPane;
  private FlowPane comboPane;

  private Label firstNameLabel;
  private Label lastNameLabel;
  private Label addressLabel;
  private Label emailLabel;
  private Label phoneNumberLabel;
  private GridPane MemberInputPane;

  private ComboBox<Member> memberBox;
  private TextField firstNameField;
  private TextField lastNameField;
  private TextField addressField;
  private TextField emailField;
  private TextField phoneNumberField;

  private RadioButton r1;
  private RadioButton r2;
  private ToggleGroup group;



/*Buttons*/
  private Button updateButton;
  private Button addButton;
  private Button removeButton;

  private MyActionListener listener;
  private  MyActionListener addListener;
  private  MyActionListener rListener;
  private MemberFileAdapter adapter;

  /**
   * Constructor initializing the GUI components
   * @param title The title of the tab
   * @param adapter StudentFileAdapter object used for retrieving and storing student information
   */
  public EditMemberTab(String title, MemberFileAdapter adapter)
  {
    super(title);

    this.adapter = adapter;

    listener = new MyActionListener();
    addListener  = new MyActionListener();
    rListener= new MyActionListener();

    updateButton = new Button("Update");
    updateButton.setOnAction(listener);

    addButton = new Button("Add member");
    addButton.setOnAction(addListener);

    removeButton = new Button("Remove member");
    removeButton.setOnAction(rListener);

    MemberPane = new VBox(30);
    MemberPane .setPadding(new Insets(20));

    MemberTopPane = new HBox(30);

    memberBox = new ComboBox<Member>();
    memberBox.setOnAction(listener);
    memberBox.setOnAction(rListener);

    comboPane = new FlowPane();
    comboPane.setAlignment(Pos.BASELINE_RIGHT);
    comboPane.setPrefWidth(200);
    comboPane.getChildren().add(memberBox);

    firstNameLabel = new Label("First name:");
    lastNameLabel = new Label("Last name:");
    addressLabel = new Label("Address:");
    emailLabel = new Label("Email:");
    phoneNumberLabel=new Label("Phone number: ");

    firstNameField = new TextField();
    firstNameField.setEditable(false);
    lastNameField = new TextField();
    lastNameField.setEditable(false);
    addressField = new TextField();
    emailField = new TextField();
    phoneNumberField=new TextField();


    MemberInputPane = new GridPane();
    MemberInputPane.setHgap(5);
    MemberInputPane.setVgap(5);
    MemberInputPane.addRow(0, firstNameLabel, firstNameField);
    MemberInputPane.addRow(1, lastNameLabel, lastNameField);
    MemberInputPane.addRow(2, addressLabel, addressField);
    MemberInputPane.addRow(3, phoneNumberLabel, phoneNumberField);
    MemberInputPane.addRow(4, emailLabel, emailField);
    r1 = new RadioButton("Regular membership");
    r2 = new RadioButton("Premium membership");
    group=new ToggleGroup();

    r1.setToggleGroup(group);
    r2.setToggleGroup(group);

    MemberInputPane.addRow(5,r1,r2);


    MemberTopPane.getChildren().add(MemberInputPane);
    MemberTopPane.getChildren().add(comboPane);


    MemberPane.getChildren().add(MemberTopPane);
    MemberPane.getChildren().add(updateButton);
    MemberPane.getChildren().add(addButton);
    MemberPane.getChildren().add(removeButton);

    super.setContent(MemberPane);
  }

  /**
   * Enables or disables editing of firstNameField and lastNameField.
   * @param bool if true then the fields will be editable, if false then they will not
   */
  public void changeEditableState(boolean bool)
  {
    firstNameField.setEditable(bool);
    lastNameField.setEditable(bool);
  }

  /**
   * Updates the studentBox ComboBox with information from the students file
   */
  public void updateMemberBox()
  {
    int currentIndex = memberBox.getSelectionModel().getSelectedIndex();

   memberBox.getItems().clear();

    MemberList members = adapter.getAllMembers();
    for (int i = 0; i < members.size(); i++)
    {
      memberBox.getItems().add(members.get(i));
    }

    if (currentIndex == -1 && memberBox.getItems().size() > 0)
    {
      memberBox.getSelectionModel().select(0);
    }
    else
    {
      memberBox.getSelectionModel().select(currentIndex);
    }
  }

  /*
   * Inner action listener class
   * @author Allan Henriksen
   * @version 3.0
   */
  private class MyActionListener implements EventHandler<ActionEvent>
  {
    public void handle(ActionEvent e)
    {
      if (e.getSource() == updateButton)
      {
        Member old = memberBox.getSelectionModel().getSelectedItem();

        String firstName = firstNameField.getText();
        String lastName = lastNameField.getText();
        String address = addressField.getText();
        String email = emailField.getText();
        String phoneNumber = phoneNumberField.getText();
        if (address.equals(""))
        {
          address = "?";
        }
        boolean premium=false;
        if(r2.isSelected()){
          premium=true;
        }

        adapter.editMember(old,firstName, lastName, address, phoneNumber, email, premium);
        updateMemberBox();

      }
      else if(e.getSource()==addButton){

        String firstName = firstNameField.getText();
        String lastName = lastNameField.getText();
        String address = addressField.getText();
        String email = emailField.getText();
        String phoneNumber = phoneNumberField.getText();
        if (address.equals(""))
        {
          address = "?";
        }
        boolean premium=false;
        if(r2.isSelected()){
          premium=true;
        }
        Member member2= new Member(firstName, lastName, address, phoneNumber, premium);
        if(!email.equals("")){
          member2.setEmail(email);
        }

        adapter.addMember(member2);
        updateMemberBox();
      }


      else if(e.getSource()==removeButton){

        String firstName = firstNameField.getText();
        String lastName = lastNameField.getText();
        String address = addressField.getText();
        String email = emailField.getText();
        String phoneNumber = phoneNumberField.getText();
        boolean premium=false;
        if(r2.isSelected()){
          premium=true;
        }
        Member member2= new Member(firstName, lastName, address, phoneNumber, premium);
        if(!email.equals("")){
          member2.setEmail(email);
        }

        adapter.removeMember(member2);
        updateMemberBox();
      }



      else if (e.getSource() == memberBox)
      {
        Member temp = memberBox.getSelectionModel().getSelectedItem();

        if (temp != null)
        {
          firstNameField.setText(temp.getFirstName());
          lastNameField.setText(temp.getLastName());
          addressField.setText(temp.getAddress());
          emailField.setText(temp.getEmail());
          phoneNumberField.setText(temp.getPhoneNumber());


          if(temp.getPremium()){
           r2.setSelected(true);
          }
          else{
            r1.setSelected(true);
          }

        }
      }


    }


  }



}
