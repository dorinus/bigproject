import javafx.application.Application;

public class Test
{
   /**
     * Starts the program
     * @param args Command line arguments
     */
    public static void main(String[] args)
    {
      Application.launch(GUI.class);
    }
  }

