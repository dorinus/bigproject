import java.io.FileNotFoundException;
import java.io.IOException;

public class MemberFileAdapter
{
  private MyFileIO mfio;
  private String fileName;

  /**
   * 1-argument constructor setting the file name.
   * @param fileName the name and path of the file where students will be saved and retrieved
   */
  public MemberFileAdapter(String fileName)
  {
    mfio = new MyFileIO();
    this.fileName = fileName;
  }

  /**
   * Uses the MyFileIO class to retrieve a StudentList object with all Students.
   * @return a StudentList object with all stored students
   */
  public MemberList getAllMembers()
  {
    MemberList members = new MemberList();

    try
    {
      members = (MemberList)mfio.readObjectFromFile(fileName);
    }
    catch (FileNotFoundException e)
    {
      System.out.println("File not found");
    }
    catch (IOException e)
    {
      System.out.println("IO Error reading file");
    }
    catch (ClassNotFoundException e)
    {
      System.out.println("Class Not Found");
    }
    return members;
  }

  /**
   * Use the MyFileIO class to retrieve all students from a given country.
   * //@param isPremium the country to retrieve students from
   * @return a StudentList object with students from the given country
   */
  public MemberList MembersPremium()
  {

    MemberList members = new MemberList();

    try
    {
      MemberList result = (MemberList)mfio.readObjectFromFile(fileName);

      for (int i = 0; i < result.size(); i++)
      {
        if (result.get(i).getPremium())
        {
          members.addMember(result.get(i));
        }
      }
    }
    catch (FileNotFoundException e)
    {
      System.out.println("File not found");
    }
    catch (IOException e)
    {
      System.out.println("IO Error reading file");
    }
    catch (ClassNotFoundException e)
    {
      System.out.println("Class Not Found");
    }

    return members;
  }

  /**
   * Use the MyFileIO class to retrieve all students from a given country.
   * //@param isRegular the address to retrieve students from
   * @return a MemberList object with students from the given country
   */
  public  MemberList MembersRegular()
  {
    MemberList members = new MemberList();

    try
    {
      MemberList result = (MemberList)mfio.readObjectFromFile(fileName);

      for (int i = 0; i < result.size(); i++)
      {
        if (!result.get(i).getPremium())
        {
          members.addMember(result.get(i));
        }
      }
    }
    catch (FileNotFoundException e)
    {
      System.out.println("File not found");
    }
    catch (IOException e)
    {
      System.out.println("IO Error reading file");
    }
    catch (ClassNotFoundException e)
    {
      System.out.println("Class Not Found");
    }

    return members;
  }
  /**
   * Use the MyFileIO class to save some students.
   * @param members the list of students that will be saved
   */
  public void saveMembers(MemberList members)
  {
    try
    {
      mfio.writeToFile(fileName, members);
    }
    catch (FileNotFoundException e)
    {
      System.out.println("File not found");
    }
    catch (IOException e)
    {
      System.out.println("IO Error writing to file");
    }
  }

  /**
   * Uses the MyFileIO class to change the country of a student with
   * the given first name and last name.
   * @param firstName the first name of the student
   * @param lastName the last name of the student
   * @param address the student's new country
   */
  public void editMember(Member member, String firstName, String lastName,
      String address, String phoneNumber, String email, boolean premium)
  {
    MemberList members = getAllMembers();

    for (int i = 0; i < members.size(); i++)
    {
      Member member1 = members.get(i);

      if (member1.equals(member))
      {
        member1.setAddress(address);
        member1.setFirstName(firstName);
        member1.setLastName(lastName);
        member1.setEmail(email);
        member1.setPhoneNumber(phoneNumber);
        member1.setPremium(premium);
      }
    }
    saveMembers(members);
  }

  public void addMember(Member member) {
    MemberList members = getAllMembers();
    boolean exists=false;

    for (int i = 0; i < members.size(); i++)
    {
      Member member1 = members.get(i);

      if (member.getPhoneNumber().equals(member1.getPhoneNumber()))
      {
        exists = true;
        break;
      }
    }
    if(!exists){
      members.addMember(member);
      saveMembers(members);
      }
    }


  public void removeMember(Member member) {
    MemberList members = getAllMembers();

    for (int i = 0; i < members.size(); i++)
    {
      Member member1 = members.get(i);

      if (member.getPhoneNumber().equals(member1.getPhoneNumber()))
      {
        members.removeMember(member);
        saveMembers(members);
      }
    }


  }



}
