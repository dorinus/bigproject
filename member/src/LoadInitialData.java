import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * A simple program used for importing data. It reads a text file with student
 * information, creates a StudentList object with all students read from the file, 
 * and saves it as a binary file. The text file must have information of one student
 * on each line, and each line should be in the format: firstName,lastName,country
 * @author Allan Henriksen
 * @version 1.0
 */
public class LoadInitialData
{
   public static void main(String[] args)
   {
      MemberList members = new MemberList();

      MyTextFileIO mtfio = new MyTextFileIO();
      String[] memberArray = null;
      try
      {
         memberArray = mtfio.readArrayFromFile("SEPmember/members.txt");
                      
         for(int i = 0; i< memberArray.length; i++)
         {
            String temp =  memberArray[i];
            String[] tempArr = temp.split(",");
            String firstName = tempArr[0];
            String lastName = tempArr[1];
            String address = tempArr[2];
            String phoneNumber = tempArr[3];
            String membership = tempArr[4];
            boolean premium=false;
            if(membership.equals("true")){
               premium=true;
            }
            members.addMember(new Member(firstName, lastName,address, phoneNumber,premium));
         }
      }
      catch (FileNotFoundException e)
      {
         System.out.println("File was not found, or could not be opened");
      }
     
      MyFileIO mfio = new MyFileIO();
      
      try
      {
         mfio.writeToFile("members.bin", members);
      }
      catch (FileNotFoundException e)
      {
         System.out.println("Error opening file ");
      }
      catch (IOException e)
      {
         System.out.println("IO Error writing to file ");
      }
      
      System.out.println("Done");
   }
}
