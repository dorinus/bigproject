import java.io.Serializable;

public class Date implements Serializable
{
  private int day;
  private int month;
  private int year;

  public Date(int day, int month, int year)
  {
    this.day = day;
    this.month = month;
    this.year = year;
  }

  public void setDate(int day, int month, int year)
  {
    this.day = day;
    this.month = month;
    this.year = year;
  }

  public int getDay()
  {
    return day;
  }

  public int getMonth()
  {
    return month;
  }

  public int getYear()
  {
    return year;
  }

 /* public boolean isLeapYear()
  {
    if (year % 4 == 0)
    {
      if (year % 400 == 0 || year % 100 != 0)
        return true;
      else
        return false;
    }
    else
      return false;
  }

  public int daysInMonth()
  {
    if(month==1 || month==3 || month==5 || month==7 || month==8 || month==10 || month==12)
    {
      return 31;
    }
    else if(month == 2 && isLeapYear())
    {
      return 29;
    }
    else if(month ==2 && ! isLeapYear())
      return 28;

    else if(month == 4 || month == 6 || month==9 || month==11)
      return 30;

    else
    {
      return -1;
    }
  } */

  public String dayOfWeek()
  {
    if (month >= 3 && month <= 12)
    {
      int k = year % 100;
      int j = year / 100;
      double h = (day + 13 * (month + 1) / 5 + k + k / 4 + j / 4 + 5 * j) % 7;

      if (h == 0)
        return "Saturday";
      else if (h == 1)
        return "Sunday";
      else if (h == 2)
        return "Monday";
      else if (h == 3)
        return "Tuesday";
      else if (h == 4)
        return "Wednesday";
      else if (h == 5)
        return "Thursday";
      else if (h == 6)
        return "Friday";
    }
    if (month == 1 || month == 2)
    {
      int m = month;
      int y = year;
      m += 12;
      y -= 1;
      int k = y % 100;
      int j = y / 100;
      double h = (day + 13 * (m + 1) / 5 + k + k / 4 + j / 4 + 5 * j) % 7;

      if (h == 0)
        return "Saturday";
      else if (h == 1)
        return "Sunday";
      else if (h == 2)
        return "Monday";
      else if (h == 3)
        return "Tuesday";
      else if (h == 4)
        return "Wednesday";
      else if (h == 5)
        return "Thursday";
      else if (h == 6)
        return "Friday";
      else
        return "Error";
    }
    else
    {
      return "Error";
    }
  }

  public Date copy()
  {
    return new Date(day, month, year);
  }

  public String toString()
  {
    return getDay() + "/" + getMonth() + "/" + getYear();
  }
}
