import java.io.Serializable;
import java.util.ArrayList;

public class Schedule implements Serializable
{
  private ArrayList<ScheduledClass> schedule;

  public Schedule()
  {
    schedule = new ArrayList<ScheduledClass>();
  }

  public ArrayList<ScheduledClass> getSchedule()
  {
    return schedule;
  }

  public void addScheduled(ScheduledClass scheduleClass)
  {
    schedule.add(scheduleClass);
  }

  public void removeSchedule(ScheduledClass scheduledClass)
  {
    schedule.remove(scheduledClass);
  }

  public ArrayList<ScheduledClass> getScheduleByClassName(Class className)
  {
    ArrayList<ScheduledClass> sc = new ArrayList<>();
    for (int i = 0; i < schedule.size(); i++)
    {
      if(schedule.get(i).getClass().equals(className))
      {
        sc.add(schedule.get(i));
      }
    }
    return sc;
  }

  //to do arange

  public String toString(){
    StringBuilder temp= new StringBuilder();
    for(ScheduledClass sc : schedule){
      temp.append(sc.toString()).append("\n");
    }
    return temp.toString();
  }
}
