import java.io.Serializable;

public abstract class Person implements Serializable
{
  private String name;
  private String address;
  private int phoneNumber;
  private String email;

  public Person(String name, String address, int number)
  {
    this.name = name;
    this. address = address;
    phoneNumber = number;
    email = null;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public void setAddress(String address)
  {
    this.address = address;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }

  public void setPhoneNumber(int number)
  {
    phoneNumber = number;
  }

  public String getName()
  {
    return name;
  }

  public String getAddress()
  {
    return address;
  }

  public String getEmail()
  {
    return email;
  }

  public int getPhoneNumber()
  {
    return phoneNumber;
  }

  public String toString()
  {
    if(email != null)
    {
      return getName() + " - " + getAddress() + " " + getEmail() + " " + getPhoneNumber();
    }
    else
    {
      return getName() + " - " + getAddress() + " " + getPhoneNumber();
    }
  }
}
