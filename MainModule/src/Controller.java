import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import java.util.ArrayList;

public class Controller
{

  public Button button;
  public TextField text;
  public ListView listView = new ListView();

  public void clickMe(ActionEvent actionEvent)
  {
    System.out.println("Button was clicked");
    button.setText("Hahahah");
  }

  public void listView()
  {

    ArrayList<String> temp= new ArrayList<>();

    ScheduleFileAdapter scheduleFileAdapter = new ScheduleFileAdapter("schedule.bin");
    Schedule schedule= scheduleFileAdapter.getSchedule();
    for (int i=0;i<schedule.getSchedule().size();i++){
      temp.add(schedule.getSchedule().get(i).getClassName().getName());
    }
    listView.getItems().addAll(temp);


    //listView.getItems().addAll(schedule.getSchedule());
  }
}
