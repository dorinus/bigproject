import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class FileIO
{
 public void write(String fileName, Object obj)
 {
  try
  {
   FileOutputStream fileOut = new FileOutputStream(fileName);
   ObjectOutputStream write = new ObjectOutputStream(fileOut);
   write.writeObject(obj);
   write.close();
  }
  catch (FileNotFoundException e)
  {
   System.out.println("File not found, or could not be opened");
   System.exit(1);
  }
  catch (IOException e)
  {
   System.out.println("IO Error writing to file ");
   System.exit(1);
  }
  System.out.println("Done writing");
 }



 public Object readObjectFromFile(String fileName)
 {
  Object obj = new Object();
  try
  {
   FileInputStream fileIn = new FileInputStream(fileName);
   ObjectInputStream read = new ObjectInputStream(fileIn);


   while (true)
   {
    try
    {
    obj=read.readObject();

    }
    catch (EOFException eof)
    {
     System.out.println("End of file");
     break;
    }
    catch (ClassNotFoundException e)
    {
     e.printStackTrace();
    }
   }
   read.close();
  }
  catch (FileNotFoundException e)
  {
   System.out.println("File not found, or could not be opened");
   System.exit(1);
  }
  catch (IOException e)
  {
   System.out.println("IO Error reading file");
   e.printStackTrace();
   System.exit(1);
  }
return obj;
 }
}


