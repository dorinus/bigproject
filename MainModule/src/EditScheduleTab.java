import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class EditScheduleTab extends Tab
{
  private VBox EditSchedulePane;
  private HBox ScheduleTopPane;
  private FlowPane comboPane;

  private Label date;
  private Label startTime;
  private Label EndTime;
  private Label instructorName;
  private Label memberName;
  private Label className;
  private GridPane MemberInputPane;

  private ComboBox<Member> memberBox;
  private DatePicker dateNameField;
  private TextField startTimeField;
  private TextField endTimeField;
  private TextField instructorField;
  private TextField memberField;
  private TextField classField;





  /*Buttons*/
  private Button updateButton;
  private Button addButton;
  private Button removeButton;

  private MyActionListener updateL;
  private  MyActionListener addMemberL;
  private  MyActionListener removeMemberL;
  private ScheduleFileAdapter adapter;

  /**
   * Constructor initializing the GUI components
   * @param title The title of the tab
   * @param adapter StudentFileAdapter object used for retrieving and storing student information
   */
  public EditScheduleTab(String title, ScheduleFileAdapter adapter)
  {
    super(title);

    this.adapter = adapter;

    updateL = new MyActionListener();
    addMemberL = new MyActionListener();
    removeMemberL = new MyActionListener();

    updateButton = new Button("Update");
    updateButton.setOnAction(updateL);

    addButton = new Button("Add member");
    addButton.setOnAction(addMemberL);

    removeButton = new Button("Remove member");
    removeButton.setOnAction(removeMemberL);

    EditSchedulePane = new VBox(30);
    EditSchedulePane.setPadding(new Insets(20));

    ScheduleTopPane = new HBox(30);

    memberBox = new ComboBox<Member>();
    memberBox.setOnAction(updateL);
    memberBox.setOnAction(removeMemberL);

    comboPane = new FlowPane();
    comboPane.setAlignment(Pos.BASELINE_RIGHT);
    comboPane.setPrefWidth(200);
    comboPane.getChildren().add(memberBox);

    date = new Label("First name:");
    startTime = new Label("Last name:");
    EndTime = new Label("Address:");
    instructorName = new Label("Email:");
    memberName =new Label("Phone number: ");

    dateNameField = new TextField();
    dateNameField.setEditable(false);
    startTimeField = new TextField();
    startTimeField.setEditable(false);
    endTimeField = new TextField();
    instructorField = new TextField();
    memberField =new TextField();


    MemberInputPane = new GridPane();
    MemberInputPane.setHgap(5);
    MemberInputPane.setVgap(5);
    MemberInputPane.addRow(0, date, dateNameField);
    MemberInputPane.addRow(1, startTime, startTimeField);
    MemberInputPane.addRow(2, EndTime, endTimeField);
    MemberInputPane.addRow(3, memberName, memberField);
    MemberInputPane.addRow(4, instructorName, instructorField);
    r1 = new RadioButton("Regular membership");
    r2 = new RadioButton("Premium membership");
    group=new ToggleGroup();

    r1.setToggleGroup(group);
    r2.setToggleGroup(group);

    MemberInputPane.addRow(5,r1,r2);


    ScheduleTopPane.getChildren().add(MemberInputPane);
    ScheduleTopPane.getChildren().add(comboPane);


    EditSchedulePane.getChildren().add(ScheduleTopPane);
    EditSchedulePane.getChildren().add(updateButton);
    EditSchedulePane.getChildren().add(addButton);
    EditSchedulePane.getChildren().add(removeButton);

    super.setContent(EditSchedulePane);
  }

  /**
   * Enables or disables editing of firstNameField and lastNameField.
   * @param bool if true then the fields will be editable, if false then they will not
   */
  public void changeEditableState(boolean bool)
  {
    dateNameField.setEditable(bool);
    startTimeField.setEditable(bool);
  }

  /**
   * Updates the studentBox ComboBox with information from the students file
   */
  public void updateMemberBox()
  {
    int currentIndex = memberBox.getSelectionModel().getSelectedIndex();

    memberBox.getItems().clear();

    MemberList members = adapter.getAllMembers();
    for (int i = 0; i < members.size(); i++)
    {
      memberBox.getItems().add(members.get(i));
    }

    if (currentIndex == -1 && memberBox.getItems().size() > 0)
    {
      memberBox.getSelectionModel().select(0);
    }
    else
    {
      memberBox.getSelectionModel().select(currentIndex);
    }
  }

  /*
   * Inner action listener class
   * @author Allan Henriksen
   * @version 3.0
   */
  private class MyActionListener implements EventHandler<ActionEvent>
  {
    public void handle(ActionEvent e)
    {
      if (e.getSource() == updateButton)
      {
        Member old = memberBox.getSelectionModel().getSelectedItem();

        String firstName = dateNameField.getText();
        String lastName = startTimeField.getText();
        String address = endTimeField.getText();
        String email = instructorField.getText();
        String phoneNumber = memberField.getText();
        if (address.equals(""))
        {
          address = "?";
        }
        boolean premium=false;
        if(r2.isSelected()){
          premium=true;
        }

        adapter.editMember(old,firstName, lastName, address, phoneNumber, email, premium);
        updateMemberBox();

      }
      else if(e.getSource()==addButton){

        String firstName = dateNameField.getText();
        String lastName = startTimeField.getText();
        String address = endTimeField.getText();
        String email = instructorField.getText();
        String phoneNumber = memberField.getText();
        if (address.equals(""))
        {
          address = "?";
        }
        boolean premium=false;
        if(r2.isSelected()){
          premium=true;
        }
        Member member2= new Member(firstName, lastName, address, phoneNumber, premium);
        if(!email.equals("")){
          member2.setEmail(email);
        }

        adapter.addMember(member2);
        updateMemberBox();
      }


      else if(e.getSource()==removeButton){

        String firstName = dateNameField.getText();
        String lastName = startTimeField.getText();
        String address = endTimeField.getText();
        String email = instructorField.getText();
        String phoneNumber = memberField.getText();
        boolean premium=false;
        if(r2.isSelected()){
          premium=true;
        }
        Member member2= new Member(firstName, lastName, address, phoneNumber, premium);
        if(!email.equals("")){
          member2.setEmail(email);
        }

        adapter.removeMember(member2);
        updateMemberBox();
      }



      else if (e.getSource() == memberBox)
      {
        Member temp = memberBox.getSelectionModel().getSelectedItem();

        if (temp != null)
        {
          dateNameField.setText(temp.getFirstName());
          startTimeField.setText(temp.getLastName());
          endTimeField.setText(temp.getAddress());
          instructorField.setText(temp.getEmail());
          memberField.setText(temp.getPhoneNumber());


          if(temp.getPremium()){
            r2.setSelected(true);
          }
          else{
            r1.setSelected(true);
          }

        }
      }


    }


  }



}
