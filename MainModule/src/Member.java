import java.io.Serializable;

public class Member implements Serializable
{
  private boolean premium;
  private String firstName;
  private String lastName;
  private String address;
  private String phoneNumber;
  private String email;

  public Member(String firstName,String lastName, String address, String phoneNumber, boolean premium)
  {
    this.firstName=firstName;
    this.lastName=lastName;
    this.address=address;
    this.phoneNumber=phoneNumber;
    this.premium = premium;
    email="";
  }

  public void setFirstName(String firstName)
  {
    this.firstName = firstName;
  }

  public String getFirstName()
  {
    return firstName;
  }

  public void setLastName(String lastName)
  {
    this.lastName = lastName;
  }

  public String getLastName()
  {
    return lastName;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }

  public String getEmail()
  {
    return email;
  }

  public void setPhoneNumber(String phoneNumber)
  {
    this.phoneNumber = phoneNumber;
  }

  public String getPhoneNumber()
  {
    return phoneNumber;
  }

  public void setAddress(String address)
  {
    this.address = address;
  }

  public String getAddress()
  {
    return address;
  }

  public void setPremium(boolean premium)
  {
    this.premium = premium;
  }

  public boolean getPremium()
  {
    return premium;
  }

  public boolean isPremium()
  {
    return premium;
  }

  public boolean equals(Object obj)
  {
    if (!(obj instanceof Member))
    {
      return false;
    }

    Member other = (Member) obj;

    return firstName.equals(other.firstName) && lastName.equals(other.lastName)
        && address.equals(other.address) && phoneNumber.equals(other.phoneNumber)
        && email.equals(other.email) && premium==other.premium;
  }

  public String toString()
  {
    if(premium)
    {
      return   " premium " +firstName +" " +
          lastName+ " "  + address +" " + phoneNumber + " " + email;
    }
    else
    {
      return   " regular " +firstName +" " +
          lastName+ " "  + address +" " + phoneNumber + " " + email;
    }
  }
}
