import java.io.Serializable;
import java.util.ArrayList;

public class Instructor extends Person implements Serializable
{
  private ArrayList<Class> classes;

  public Instructor(String name, String address, int number)
  {
    super(name, address, number);
    classes = new ArrayList<Class>();
  }

  public void addClass(Class clas)
  {
    classes.add(clas);
  }

  public void removeClass(Class clas)
  {
    classes.remove(clas);
  }

  public ArrayList<Class> getAllClasses()
  {
    return classes;
  }

  public String toString()
  {
    return super.toString() + " " + getAllClasses();
  }
}
