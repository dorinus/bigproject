import java.io.Serializable;

public class Time implements Serializable
{
  private int hour;
  private int minute;
  private int second;

  public Time(int hour, int minute, int sec)
  {
    this.hour = hour;
    this.minute = minute;
    second = sec;
  }

  public Time(int timeInSec)
  {
    hour = timeInSec/3600;
    timeInSec -= hour*3600;

    minute = timeInSec/60;
    timeInSec -= minute*60;

    second = timeInSec;
  }

  public void setTime(int hour, int minute, int sec)
  {
    this.hour = hour;
    this.minute = minute;
    second = sec;
  }

  public int getHour()
  {
    return hour;
  }

  public int getMinute()
  {
    return minute;
  }

  public int getSecond()
  {
    return second;
  }

  public int convertToSec()
  {
    return hour*3600 + minute*60 + second;
  }

  public boolean isBeforeTime(Time time)
  {
    if(hour<time.hour)
    {
      return true;
    }
    else if (hour>time.hour)
    {
      return false;
    }
    else
    {
      if (minute < time.minute)
      {
        return true;
      }
      else if (minute > time.minute)
      {
        return false;
      }
      else
      {
        if (second < time.second)
          return true;
        else
          return false;
      }
    }
  }

  public Time timeTo(Time time)
  {
    int sec;
    if (convertToSec()>time.convertToSec())
      sec = convertToSec() - time.convertToSec();
    else
      sec = time.convertToSec() - convertToSec();

    return new Time(sec);
  }

  public Time copy()
  {
    return new Time(hour, minute, second);
  }

  public String toString()
  {
    return getHour() + ":" + getMinute() + ":" + getSecond();
  }
}
