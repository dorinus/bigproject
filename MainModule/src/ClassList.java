import java.util.ArrayList;

public class ClassList
{
  private ArrayList<Class> classes;

  public ClassList()
  {
    classes = new ArrayList<>();
  }

  public void addClass(Class className)
  {
    classes.add(className);
  }

  public void removeClass(Class className)
  {
    classes.remove(className);
  }

  public Class show(String name)
  {
    Class temp = null;
    for (int i = 0; i < classes.size(); i++)
    {
      if(classes.get(i).getName().equals(name))
      {
        temp = classes.get(i);
        break;
      }
    }
    return temp;
  }

  public String toString()
  {
    String str = "";
    for (int i = 0; i < classes.size(); i++)
    {
      str += classes.get(i) + "\n";
    }
    return str;
  }
}
