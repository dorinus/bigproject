import java.io.Serializable;
import java.util.ArrayList;

public class ScheduledClass implements Serializable
{
  private int maxParticipants;
  private Date date;
  private Time start;
  private Time end;
  private Class className;
  private Instructor instructor;
  private ArrayList<Member> members;

  public ScheduledClass(int maxParticipants, Date date, Time start, Time end, Class className, Instructor instructor)
  {
    this.date = date.copy();
    this.start = start.copy();
    this.end = end.copy();
    this.className = className;
    this.instructor = instructor;
    members = new ArrayList<Member>(maxParticipants);
  }

  public int getMaxParticipants()
  {
    return maxParticipants;
  }

  public void addMember(Member member)
  {
    if(member.isPremium())
    {
      members.add(member);
    }
  }

  public void removeMember(Member member)
  {
    members.remove(member);
  }

  public Date getDate()
  {
    return date.copy();
  }

  public Class getClassName()
  {
    return className;
  }

  public String toString()
  {
    String str = "";
    for (int i = 0; i < members.size(); i++)
    {
      str += members.get(i) + "\n";
    }
    return className + " - " + date + " " + start + " - " + end + "\nInstructor: " + instructor +
        "\nList of members:\n" + str;
  }
}
